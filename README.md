# AISFEEL (AIS FEature Engineering Library)

Im Modul Cleaning befinden sich Funktionen, mit denen AIS (ais_cleaning) und Wetter-Daten (weather_cleaning) bereinigt werden können.
In datasets befinden sich Funktionen, für das Downloaden von Besipieldatensätzen.
In extraction befinden sich die Funktionen für das berechnen von Zusätzlichen Features. Filter Selection und Zeitreihenextraction sind dort ebenfalls implementiert.
In selection befindet sich die Funktion für SFBS.

# Installation
Für das Ausführen der Notebooks müssen die Dependencies, die in der pyproject.toml angegeben sind, installiert werden.
[Poetry](https://python-poetry.org/) wurde für das dependency Management verwendet. 
Mit >poetry install können die abhängigkeiten installiert werden. Alternativ ist auch eine requirements.txt enthalten. Die Abhängigkeiten für Geopandas für Windows sind im ordner dependency_binaries hinterlegt, da es beim installieren oft zu Problemen kommt.
Python 3.8.2 muss installiert sein. 

## Beispiel und Evaluation Notebooks 
In Notebooks sind eine Reihe von Beispielen für die verwendung enthalten (Leider noch größtenteils unkommentiert). 
Die Evaluation wurde in den Notebooks, die mit der Ziffer 09 Beginnen, Schrittweise durchgeführt. Mit Jupyter lab ('jupyter lab' in der console) ist es am einfachsten, diese nacheinander auszuführen.

- In 09_1_Evaluation_data_collection_cleaning_integration werden die Datensätze heruntergeladen und bereinigt.
- In 09_2a_Evaluation_timeseries_extraction.ipynb wird der Datensatz D_2 erstellt.
- In 09_3a_Evaluation_Modell_DTR_0_und_DTR_1 werden die Modelle DTR_0 und DTR_1 evaluiert.
- In 09_3b_Evaluation_Modell_DTR_1_ohne_speed_ableitungen wird DTR_1 ohne die von sog abgeleiteten features überprüft.
- In 09_3c_Evaluation_Modell_DTR_2 wird dann DTR_2 evaluiert. 

# Enhancements

- use fsspec for ftp download