import pandas as pd
import pathlib
import joblib
import pytest

from aisfeel.cleaning import ais_cleaning


normalized_path = pathlib.Path("tests/data/normalized.joblib")
raw_path = pathlib.Path("tests/data/testdata.joblib")


@pytest.fixture
def raw():
    return joblib.load(raw_path)


@pytest.fixture
def normalized():
    return joblib.load(normalized_path)


def test_mmsi_filter(normalized):
    mmsis = normalized.mmsi

    filtered = mmsis[mmsis.isin(ais_cleaning.mmsi_filter(mmsis))]

    assert len(filtered) < len(mmsis)
    assert len(filtered.max().astype(str)) == 9
    assert len(filtered.min().astype(str)) == 9
