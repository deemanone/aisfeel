from aisfeel import DEFAULT_CONFIGURATION
import asyncio


def download_weather(configuration=DEFAULT_CONFIGURATION):
    asyncio.get_event_loop().run_until_complete(_download_all())
    for dataset in configuration.c:
        pass


def _get_templates(configuration):

    template_command_current = f"""python -m motuclient --motu http://nrt.cmems-du.eu/motu-web/Motu \
    --service-id GLOBAL_ANALYSIS_FORECAST_PHY_001_024-TDS \
    --product-id global-analysis-forecast-phy-001-024-hourly-t-u-v-ssh \
    --longitude-min 3 --longitude-max 17 --latitude-min 53 --latitude-max 59 \
    --date-min "2020-12-31 23:30:00" --date-max "2021-03-01 00:30:00" \
    --depth-min 0.493 --depth-max 0.4942 \
    --variable thetao --variable uo --variable vo --variable zos \
    --out-dir "{configuration.datasets_folder}" --out-name "current_20210101_20210301" --user "{configuration.copernicus_secrets.username}" --pwd "{configuration.copernicus_secrets.password}"
"""

    template_command_wind = f"""python -m motuclient --motu http://nrt.cmems-du.eu/motu-web/Motu \
    --service-id WIND_GLO_WIND_L4_NRT_OBSERVATIONS_012_004-TDS \
    --product-id CERSAT-GLO-BLENDED_WIND_L4-V6-OBS_FULL_TIME_SERIE \
    --longitude-min 3 --longitude-max 17 --latitude-min 53 --latitude-max 59 \
    --date-min "2021-01-01 00:00:00" --date-max "2021-03-01 00:00:00" \
    --variable eastward_wind --variable eastward_wind_rms --variable height --variable northward_wind --variable northward_wind_rms --variable sampling_length \
    --variable surface_downward_eastward_stress --variable surface_downward_northward_stress --variable surface_type --variable wind_speed --variable wind_speed_rms \
    --variable wind_stress --variable wind_stress_curl --variable wind_stress_divergence \
    --variable wind_vector_curl --variable wind_vector_divergence \
    --out-dir {configuration.datasets_folder} --out-name "wind_20210101_20210301.nc" --user {configuration.copernicus_secrets.username} --pwd {configuration.copernicus_secrets.password}"""

    template_command_wave = f"""python -m motuclient --motu http://nrt.cmems-du.eu/motu-web/Motu \
    --service-id GLOBAL_ANALYSIS_FORECAST_WAV_001_027-TDS \
    --product-id global-analysis-forecast-wav-001-027 \
    --longitude-min 3 --longitude-max 17 --latitude-min 53 --latitude-max 60 \
    --date-min "2021-01-01 00:00:00" --date-max "2021-03-01 00:00:00" \
    --variable VHM0 --variable VHM0_SW1 --variable VHM0_SW2 --variable VHM0_WW --variable VMDR --variable VMDR_SW1 --variable VMDR_SW2 --variable VMDR_WW --variable VPED --variable VSDX \
    --variable VSDY --variable VTM01_SW1 --variable VTM01_SW2 --variable VTM01_WW --variable VTM02 --variable VTM10 --variable VTPK \
    --out-dir {configuration.datasets_folder} --out-name "wave_20210101_20210301.nc" --user {configuration.copernicus_secrets.username} --pwd {configuration.copernicus_secrets.password}"""

    return template_command_current, template_command_wave, template_command_wind


def _get_dataset_var_mapping(dataset):
    mapping = dict()
    for x in dataset.data_vars.keys():
        attrs = dataset[x].attrs
        mapping[x] = f"{attrs.get('standard_name')}_unit_{attrs.get('units')}"
    return mapping


async def _run(command):
    proc = await asyncio.create_subprocess_shell(
        command, stderr=asyncio.subprocess.PIPE
    )
    _, stderr = await proc.communicate()
    print(f"[exited with {proc.returncode}]")
    if proc.returncode != 0 and stderr:
        print(f"[stderr]\n{stderr.decode()}")


async def _download_all(configuration):
    tasks = [_run(cmd) for cmd in _get_templates(configuration)]
    await asyncio.gather(*tasks)
