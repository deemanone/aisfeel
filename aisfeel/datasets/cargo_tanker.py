from multiprocessing import Value
import pathlib
from sys import path
import pandas as pd
import dask.dataframe as dd
import dask.array as da
import xarray as xr
from aisfeel.extraction.spatial_calculators import vect_to_h3
from typing import List, Union
from tqdm import tqdm
from aisfeel.cleaning.clean import clean_ais, clean_weather


def merge_ais_with_weather(
    df: Union[pd.DataFrame, dd.DataFrame], weather: pd.DataFrame
):
    """
    TODO: Refactor into integration module?!?
    Merge AIS and Weather datasets based on h3 hexagon with resolution 5
    Assumes there is a timestamp field in all Frames
    """
    if isinstance(df, pd.DataFrame):
        d_funcs = pd

    elif isinstance(df, dd.DataFrame):
        d_funcs = dd
        df = df.set_index("timestamp")
    else:
        raise ValueError("Not a pandas or dask DataFrame")

    df["h3_res_5"] = vect_to_h3(df, 5)
    weather["h3_res_5"] = vect_to_h3(weather, 5)
    drop_cols = ["latitude", "longitude"]

    weather = weather.drop(drop_cols, axis=1)
    weather = weather.set_index("timestamp").sort_index()

    return d_funcs.merge_asof(
        df,
        weather,
        left_index=True,
        right_index=True,
        by="h3_res_5",
    ).reset_index()
    # .drop_duplicates()


def clean_and_merge_datasets(
    aisdf: Union[dd.DataFrame, pd.DataFrame],
    wind: pathlib.Path,
    wave: pathlib.Path,
    current: pathlib.Path,
):
    """
    Protoype uses wind, wave and current datasets. Convenience Function to do cleaning and integration of ais with weather data
    :aisfiles: list of csv files with aisdata
    :wind:, :wave:, :current: netcdf files with weatherdata
    :outdir: output directory for the parquet filestore
    """
    wind, wave, current = (
        clean_weather(xr.load_dataset(wind)),
        clean_weather(xr.load_dataset(wave)),
        clean_weather(xr.load_dataset(current)),
    )

    return (
        aisdf.pipe(clean_ais)
        .pipe(select_cargo_tanker)
        .pipe(merge_ais_with_weather, wind)
        .pipe(merge_ais_with_weather, wave)
        .pipe(merge_ais_with_weather, current)
        .pipe(fill_missing)
    )


def fill_missing(df):
    return df.groupby("mmsi").apply(lambda group: group.ffill().bfill()).dropna()


def select_cargo_tanker(
    df: Union[pd.DataFrame, dd.DataFrame]
) -> Union[pd.DataFrame, dd.DataFrame]:
    """
    Select only Cargo and Tanker ships, that are not at anchor or moored and drop unwanted columns
    """
    return (
        df.query(
            "(ship_type == 'Cargo' | ship_type == 'Tanker') & (type_of_mobile == 'Class A')"
        )
        .pipe(select_nav_status)
        .pipe(drop_not_needed)
    )


def select_nav_status(
    df: Union[pd.DataFrame, dd.DataFrame]
) -> Union[pd.DataFrame, dd.DataFrame]:
    """
    Filter out Ships that are at anchor, moored or have not set their nav_status
    """
    # status = ["At anchor", "Moored"]
    return df.query("~(navigational_status.isin(['At anchor', 'Moored']))")


def drop_not_needed(
    df: Union[pd.DataFrame, dd.DataFrame]
) -> Union[pd.DataFrame, dd.DataFrame]:
    drop_cols = [
        "name",
        "type_of_position_fixing_device",
        "destination",
        "data_source_type",
        "callsign",
        "imo",
    ]
    return df.drop(columns=drop_cols)