"""
Contains data from the Danish Maritime Authority that is used in accordance with the conditions for the use of Danish public data.
Downloads a Dataset for one day from ftp://ftp.ais.dk/ais_data/ (I UPLOADED IT TO MY UNI DRIVE for easier download)
"""
import pathlib
import tempfile
import requests
import asyncio


import pandas as pd
import aioftp
import pendulum
import logging
from aisfeel.config import Configuration

logger = logging.getLogger(__name__)


URL = "ftp.ais.dk"


def parse_list_line_custom(line: bytes):
    """custom line parsing function because the aisdk ftp server is weird"""
    size_index = -2
    datei_index = -1
    date_index = 0
    time_index = 2
    dir_marker = "<DIR>"

    line = line.decode().rstrip("\r\n").split(" ")

    datetime = pendulum.from_format(
        f"{line[date_index]} {line[time_index]}", "MM-DD-YY HH:mmA"
    )

    info = {}

    info["modify"] = datetime.strftime("%m/%d/%Y %I:%M %p")

    if dir_marker in line:
        info["type"] = "dir"
    else:
        info["type"] = "file"
        info["size"] = line[size_index]

    return pathlib.PurePosixPath(line[datei_index]), info


async def download_one(data_home, filename=""):
    async with aioftp.Client.context(
        URL, parse_list_line_custom=parse_list_line_custom
    ) as client:
        await client.change_directory("ais_data")
        await client.download(filename, data_home / filename, write_into=True)


#         print(await client.exists(filename))


async def fetch_aisdk(data_home):
    # download csvs for january 2021
    fileformat = "aisdk_{YYYYMMDD}.csv"
    files = [fileformat.format(YYYYMMDD=f"2021010{n}") for n in range(1, 10)]
    files.extend([fileformat.format(YYYYMMDD=f"202101{n}") for n in range(10, 32)])

    tasks = [
        asyncio.create_task(download_one(filename=task, data_home=data_home))
        for task in files
    ]
    await asyncio.wait(tasks)


# def make_cargo_tanker_dataset(data_home=None):
#     if data_home is None:
#         data_home = Configuration().aisfeel_home

#     logger.info("Beginning download of aisdk dataset")
#     asyncio.get_event_loop().run_until_complete(fetch_aisdk(data_home=data_home))

#     logger.info("Download finished. Preparing Dataset ...")
#     for file in pathlib.Path(data_home / "aisdk").rglob("*.csv"):
#         df = pd.read_csv(file)
#         df = df
