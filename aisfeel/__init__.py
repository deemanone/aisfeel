from aisfeel import config

DEFAULT_CONFIGURATION = config.Configuration()

from aisfeel.cleaning.clean import clean_ais, clean_weather
from aisfeel.datasets.cargo_tanker import (
    clean_and_merge_datasets,
    merge_ais_with_weather,
)
from aisfeel.datasets.aisdk import fetch_aisdk
from aisfeel.extraction import (
    ais_calculators,
    spatial_calculators,
    timeseries_calculators,
)
from aisfeel.extraction.extract import extract_all
from aisfeel.extraction.timeseries_calculators import (
    timeseries_relevant_features_extractor,
)

__version__ = "0.1.0"
