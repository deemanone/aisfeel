import pathlib
from dataclasses import dataclass
import os


@dataclass
class AisColumnConfiguration:
    timestamp: str = "timestamp"
    type_of_mobile: str = "type_of_mobile"
    mmsi: str = "mmsi"
    latitude: str = "latitude"
    longitude: str = "longitude"
    navigational_status: str = "navigational_status"
    rot: str = "rot"
    sog: str = "sog"
    cog: str = "cog"
    heading: str = "heading"
    imo: str = "imo"
    callsign: str = "callsign"
    name: str = "name"
    ship_type: str = "ship_type"
    cargo_type: str = "cargo_type"
    width: str = "width"
    length: str = "length"
    type_of_position_fixing_device: str = "type_of_position_fixing_device"
    draught: str = "draught"
    destination: str = "destination"
    eta: str = "eta"
    data_source_type: str = "data_source_type"
    a: str = "a"
    b: str = "b"
    c: str = "c"
    d: str = "d"


@dataclass
class CopernicusConfiguration:
    username: str
    password: str


@dataclass
class Configuration:
    ais_columns: AisColumnConfiguration = AisColumnConfiguration()
    aisfeel_home: pathlib.Path = pathlib.Path().home() / ".aisfeel"
    datasets_folder: pathlib.Path = aisfeel_home / "datasets"
    copernicus_secrets: CopernicusConfiguration = None
