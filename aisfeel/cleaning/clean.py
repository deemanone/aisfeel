import pandas as pd
import xarray as xr
from aisfeel.cleaning import ais_cleaning, weather_cleaning


def clean_weather(ds=xr.Dataset):
    return weather_cleaning.to_dataframe(ds).pipe(weather_cleaning.weather_transform)


def clean_ais(df: pd.DataFrame):
    return (
        df.pipe(ais_cleaning.normalize_columns)
        .pipe(ais_cleaning.filter_mmsi)
        .pipe(ais_cleaning.to_timeseries)
        .pipe(ais_cleaning.drop_na)
        .pipe(ais_cleaning.ais_fillna)
        .pipe(ais_cleaning.filter_defaults)
        .pipe(ais_cleaning.select_polygon)
    )
