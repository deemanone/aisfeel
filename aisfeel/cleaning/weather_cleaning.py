import pandas as pd
import xarray as xr


def get_dataset_var_mapping(dataset: xr.Dataset) -> dict:
    mapping = dict()
    for x in dataset.data_vars.keys():
        attrs = dataset[x].attrs
        new_name = attrs.get("standard_name")
        if new_name is None:
            new_name = "_".join(attrs.get("long_name").split(" "))
        mapping[x] = f"{new_name}_unit_{attrs.get('units')}"
    return mapping


def drop_single_valued(df: pd.DataFrame) -> pd.DataFrame:
    nunique = df.apply(pd.Series.nunique)
    cols_to_drop = nunique[nunique == 1].index
    return df.drop(cols_to_drop, axis=1)


def drop_null(df: pd.DataFrame) -> pd.DataFrame:
    subset = pd.Series(
        [
            "wind_speed_unit_m_s-1",
            "sea_surface_primary_swell_wave_significant_height_unit_m",
            "northward_sea_water_velocity_unit_m_s-1",
            "eastward_sea_water_velocity_unit_m_s-1",
        ]
    )
    subset = subset[subset.isin(df.columns)].to_list()
    df = df.dropna(subset=subset)
    nunique = df.isna().apply(pd.Series.nunique)
    drop_cols = nunique[nunique == 2].index
    return df.drop(drop_cols, axis=1)


def normalize_columns(df: pd.DataFrame) -> pd.DataFrame:
    def clean_column(col: str) -> str:
        col = col.lower().replace(" ", "_")
        return col

    return df.rename(columns=lambda x: clean_column(x))


def to_dataframe(weather_dataset: xr.Dataset) -> pd.DataFrame:
    col_mapping = get_dataset_var_mapping(weather_dataset)
    return (
        weather_dataset.to_dataframe()
        .rename(columns=col_mapping)
        .reset_index()
        .rename(columns={"time": "timestamp", "lat": "latitude", "lon": "longitude"})
    )


def weather_transform(df: pd.DataFrame) -> pd.DataFrame:
    return df.pipe(normalize_columns).pipe(drop_single_valued).pipe(drop_null)
