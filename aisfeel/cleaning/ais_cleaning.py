from typing import Union
import dask.dataframe as dd
import pandas as pd
import re


def filter_mmsi(
    df: Union[pd.DataFrame, dd.DataFrame]
) -> Union[pd.DataFrame, dd.DataFrame]:
    """
    Filters MMSIs, where len(MMSi)<9
    """

    return df[df.mmsi.astype(str).str.len() == 9]


def normalize_columns(
    df: Union[pd.DataFrame, dd.DataFrame]
) -> Union[pd.DataFrame, dd.DataFrame]:
    """
    Normalize column names: No Whitespace only Alphanumeric
    """

    def clean_column(col: str):

        col = re.sub(r"[^a-zA-Z\d\s_]", "", col)
        col = re.sub(r"^\s", "", col)
        col = col.lower().replace(" ", "_")
        return col

    return df.rename(columns=lambda x: clean_column(x))


def to_timeseries(
    df: Union[pd.DataFrame, dd.DataFrame]
) -> Union[pd.DataFrame, dd.DataFrame]:
    """
    Parse the timestamp field and set a Multiindex with timestamp and mmsi
    """
    if isinstance(df, pd.DataFrame):

        df.loc[:, "timestamp"] = pd.to_datetime(
            df.timestamp, infer_datetime_format=True, cache=True
        )
        df.loc[:, "eta"] = pd.to_datetime(
            df.eta, infer_datetime_format=True, cache=True
        )

    elif isinstance(df, dd.DataFrame):
        df["timestamp"] = dd.to_datetime(df.timestamp)
        df["eta"] = dd.to_datetime(df.eta)
    else:
        raise ValueError("Not a pandas or dask DataFrame")

    return df


def drop_na(df: Union[pd.DataFrame, dd.DataFrame]) -> Union[pd.DataFrame, dd.DataFrame]:
    """
    TODO: Refactor the lists so that the col names are dependend of configuration
    Drop null values from the :subset: columns
    Drop duplicate rows
    Drop Columns that are in :drop_cols:
    """
    subset = [
        "sog",
        "cog",
        "heading",
        "latitude",
        "longitude",
        "draught",
        "width",
        "length",
        "a",
        "b",
        "c",
        "d",
        "eta",
    ]

    return df.dropna(subset=subset)
    # .drop_duplicates()


def ais_fillna(
    df: Union[pd.DataFrame, dd.DataFrame]
) -> Union[pd.DataFrame, dd.DataFrame]:
    """
    Fill missing cargo type values with the standard "No additional information"
    and fill missing rot with 0.0, assuming the rot is 0 if it is missing.
    """
    if isinstance(df, pd.DataFrame):
        df.loc[:, "rot"] = df.rot.fillna(0.0)
        df.loc[:, "cargo_type"] = df.cargo_type.fillna("No additional information")

    elif isinstance(df, dd.DataFrame):
        df["rot"] = df.rot.fillna(0.0)
        df["cargo_type"] = df.cargo_type.fillna("No additional information")
    else:
        raise ValueError("Not a pandas or dask DataFrame")
    return df


def filter_defaults(
    df: Union[pd.DataFrame, dd.DataFrame]
) -> Union[pd.DataFrame, dd.DataFrame]:
    """
    Filter out Default AIS Values
    (Refer to https://www.navcen.uscg.gov/?pageName=AISMessagesA#_Reference_point_for) for more info
    """
    return (
        df.query("0 < sog < 30")
        .query("0 <= heading < 360")
        .query("0 <= cog < 360")
        .query("-180 <= longitude <= 180")
        .query("-90 <= latitude <= 90")
        .query("-128 < rot < 128 ")
    )


def select_polygon(df: pd.DataFrame) -> pd.DataFrame:
    return df.query("(3 <= longitude <= 17) & (53 <= latitude <= 60)")
