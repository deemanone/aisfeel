from mlxtend.feature_selection import SequentialFeatureSelector as SFS
from mlxtend.plotting import plot_sequential_feature_selection as plot_sfs
import pandas as pd
import matplotlib.pyplot as plt


def select_features(model, X, y, k_features=10, cv=5, n_jobs=54, fixed_features=None):
    """Returns the SequentialFeatureSelector Object and the Model trained with the best Feature Subset.
    The Selector can be used to transform X via the transform() method in such a way,
    that only the k best Features are left."""

    sbfs = SFS(
        model,
        k_features=10,
        forward=False,
        floating=True,
        cv=5,
        n_jobs=54,
        fixed_features=fixed_features,
    )

    sbfs.fit(X, y)

    X_ = sbfs.transform(X)
    model.fit(X_)

    return sbfs, model


def show_results(sfs, ylim=[0, 1], xlim=None):
    scores = pd.DataFrame.from_dict(sfs.get_metric_dict()).T
    scores.sort_values("avg_score", inplace=True, ascending=False)
    fig1 = plot_sfs(sfs.get_metric_dict(), kind="std_dev", figsize=(12, 8))

    plt.ylim(ylim)
    if xlim:
        plt.xlim(xlim)
    plt.title("Sequential Floating Backward Selection (mit Standard Abweichungen)")
    plt.grid()
    plt.show()

    print("Best accuracy score: %.2f" % sfs.k_score_)
    print("Best subset (indices):", sfs.k_feature_idx_)
    print("Best subset (corresponding names):", sfs.k_feature_names_)

    return scores