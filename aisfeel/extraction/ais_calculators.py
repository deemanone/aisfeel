from multiprocessing import Value
from typing import Union
import pandas as pd
import dask.dataframe as dd
import dask.array as da
import numpy as np


def ais_one_hot_encoding(
    df: Union[pd.DataFrame, dd.DataFrame]
) -> Union[pd.DataFrame, dd.DataFrame]:
    categoricals = ["ship_type", "cargo_type", "navigational_status"]
    if isinstance(df, dd.DataFrame):
        d_funcs = dd
        df = df.categorize(columns=categoricals)
    elif isinstance(df, pd.DataFrame):
        d_funcs = pd

    return d_funcs.get_dummies(df, columns=categoricals)


def get_last_message(
    df: Union[pd.DataFrame, dd.DataFrame]
) -> Union[pd.DataFrame, dd.DataFrame]:
    shifted = df.groupby("mmsi")["timestamp"].shift(1).bfill()
    df["time_delta_since_last_message"] = (df.timestamp - shifted) / pd.Timedelta(
        seconds=1
    )
    return df


def is_sailing(
    df: Union[pd.DataFrame, dd.DataFrame]
) -> Union[pd.DataFrame, dd.DataFrame]:
    """
    Adds sailing status as a feature
    If the vessel is sailing, is_sailing=1, else 0
    """

    def partition_wise(df):
        vect_is_sailing = np.vectorize(_is_sailing)

        return df.sog.apply(vect_is_sailing)

    if isinstance(df, pd.DataFrame):
        df["is_sailing"] = partition_wise(df.sog.values)

    elif isinstance(df, dd.DataFrame):
        df["is_sailing"] = df.map_partitions(partition_wise)

    else:
        raise ValueError("Not a pandas or dask DataFrame")

    return df


def _is_sailing(sog, threshold=3):
    """
    max Loitering threshold defined as vessel moving under 5kt (Cazzanti und Pallotta 2015)
    https://globalfishingwatch.org/faqs/what-are-loitering-events-in-the-carrier-vessel-portal/ -> uses average of 2kt
    Prameters
    sog: sog values of vessel
    threshold: threshold below which vessel is considered as not sailing
    """
    if sog < threshold:
        return 0
    else:
        return 1


def normalize_size(
    df: Union[pd.DataFrame, dd.DataFrame]
) -> Union[pd.DataFrame, dd.DataFrame]:
    lengths = df.length
    max_length = df.length.max()
    widths = df.width
    max_width = df.width.max()
    df["norm_length"] = lengths / max_length
    df["norm_width"] = widths / max_width
    return df


def _calc_m(cog):
    if 0 <= cog < 90:
        return 90 - cog
    if 90 <= cog <= 180:
        return 90 + cog
    if 180 <= cog < 270:
        return 270 - cog
    if 270 <= cog < 360:
        return 270 + cog


def angle_to_vec(
    df: Union[pd.DataFrame, dd.DataFrame]
) -> Union[pd.DataFrame, dd.DataFrame]:
    def partition_wise(df):
        vect_calc_m = np.vectorize(_calc_m)

        return df.cog.apply(vect_calc_m)

    if isinstance(df, pd.DataFrame):
        array_funcs = np
        m = partition_wise(df)

    elif isinstance(df, dd.DataFrame):
        array_funcs = da
        m = df.map_partitions(partition_wise)

    else:
        raise ValueError("Not a pandas or dask DataFrame")

    v = df.sog
    vx = array_funcs.cos(m) * v
    vy = array_funcs.sin(m) * v
    df["vx"] = vx
    df["vy"] = vy
    return df


def drift_angle(
    df: Union[pd.DataFrame, dd.DataFrame]
) -> Union[pd.DataFrame, dd.DataFrame]:
    df["drift_angle"] = df.heading - df.cog
    return df


def u_v_ship(
    df: Union[pd.DataFrame, dd.DataFrame]
) -> Union[pd.DataFrame, dd.DataFrame]:
    """U and V (longitudinal and latitudinal components of Speed) vectors.
    Used in (S.‑H. Kim et al. 2020).
    Current u and v to correct speed.
    """
    if isinstance(df, pd.DataFrame):
        array_funcs = np
    elif isinstance(df, dd.DataFrame):
        array_funcs = da
    else:
        raise ValueError("Not a pandas or dask DataFrame")

    heading = df.heading.values
    V = df.sog.values
    df["u_ship"] = V * array_funcs.sin(heading)
    df["v_ship"] = V * array_funcs.cos(heading)

    return df
