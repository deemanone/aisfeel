"""
To Extract Timeseries Features from AIS-Data in a meaningful way with TSFresh, 
the dataset has to get resampled first. 
Next, rolling timeframes need to be extracted and last the feature extraction can happen.
The expected format is a dataframe with a Multiindex of [timeframe, mmsi]. During the evaluation, ship speed is the target vector. 
Therefore derived features are not included in the rolling and extraction.
"""
from tsfresh import extract_features, select_features
from tsfresh.utilities.dataframe_functions import roll_time_series, impute
from tsfresh.feature_extraction import EfficientFCParameters
import pandas as pd


def resample_dataset(df: pd.DataFrame):
    return df.groupby("mmsi").resample("10Min").mean().interpolate()


def make_rolls(
    df: pd.DataFrame,
    dont_include=["vx", "vy", "u_ship", "v_ship"],
    max_timeshift=5,
    min_timeshift=2,
    n_jobs=54,
):
    df = df.reset_index(drop=True)
    return roll_time_series(
        df.drop(columns=dont_include),
        column_id="mmsi",
        column_sort="timestamp",
        max_timeshift=max_timeshift,
        min_timeshift=min_timeshift,
        n_jobs=n_jobs,
    )


def timeseries_relevant_features_extractor(
    df: pd.DataFrame,
    y="sog",
    extract_columns=["cog", "heading"],
    roll=False,
    dont_include=["vx", "vy", "u_ship", "v_ship"],
    max_timeshift=5,
    min_timeshift=2,
    n_jobs=54,
):
    """
    This method rolls, extracts and selects relevant features based on the proposed method in tsfresh.
    Can take a very long time and take up a lot of memory.
    Parameters:
    :df: The dataframe including X and y columns
    :y: The Target columns name
    :extract_columns: a list of column names, of which features shall be extracted
    :roll: indicates if the df is already in rolled format or not.
    :max_timeshift: Maximum size of a sliding Window.
    :min_timeshift: Minimum size of a sliding Window.
    :n_jobs: Number of Jobs the extraction shall be distributed to. Default is 54, optimized for the Karp Workstation which has 64 cores.
    """
    extractor_settings = EfficientFCParameters()
    del extractor_settings["binned_entropy"]

    if roll:
        rolled = make_rolls(
            df,
            dont_include=dont_include,
            max_timeshift=max_timeshift,
            min_timeshift=min_timeshift,
            n_jobs=n_jobs,
        )
    else:
        rolled = df

    X = extract_features(
        rolled.drop(columns=[y, "mmsi"])[["id", "timestamp", *extract_columns]],
        column_id="id",
        column_sort="timestamp",
        n_jobs=n_jobs,
        default_fc_parameters=extractor_settings,
        show_warnings=False,
        impute_function=impute,
    )

    index = X.index.set_names(["mmsi", "timestamp"])
    X.index = index

    df = df.set_index(["mmsi", "timestamp"])

    joined = X.join(df)
    joined = joined[~joined.index.duplicated(keep="first")]

    target = joined[y]
    X = select_features(X, target, n_jobs=n_jobs)

    joined = X.join(df)
    joined = joined[~joined.index.duplicated(keep="first")]

    # X = joined.drop(y, axis=1)
    # y = joined[y]

    return joined
