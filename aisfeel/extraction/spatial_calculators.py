from typing import Union
import numpy as np
import pandas as pd
import dask.dataframe as dd
import dask.array as da
import h3.unstable.vect as vect_h3
from pandas.core.algorithms import isin

# Principal components of lat and lon

# hexagons (h3) different resolutions
"""https://h3geo.org/docs/core-library/restable
H3 Resolution	Average Hexagon Area (km2)	Average Hexagon Edge Length (km)	Number of unique indexes
0	4,250,546.8477000	1,107.712591000	122
1	607,220.9782429	418.676005500	842
2	86,745.8540347	158.244655800	5,882
3	12,392.2648621	59.810857940	41,162
4	1,770.3235517	22.606379400	288,122
5	252.9033645	8.544408276	2,016,842
6	36.1290521	3.229482772	14,117,882
7	5.1612932	1.220629759	98,825,162
8	0.7373276	0.461354684	691,776,122
9	0.1053325	0.174375668	4,842,432,842
10	0.0150475	0.065907807	33,897,029,882
11	0.0021496	0.024910561	237,279,209,162
12	0.0003071	0.009415526	1,660,954,464,122
13	0.0000439	0.003559893	11,626,681,248,842
14	0.0000063	0.001348575	81,386,768,741,882
15	0.0000009	0.000509713	569,707,381,193,162
"""


def calc_hex_res(df: Union[pd.DataFrame, dd.DataFrame]) -> pd.DataFrame:
    """
    Calculate h3 resolutions 4-9 for given dataframe
    """

    resolutions = [4, 6, 7, 8, 9]
    for res in resolutions:
        df[f"h3_res_{res}"] = vect_to_h3(df, res)
    return df


def vect_to_h3(df: Union[pd.DataFrame, dd.DataFrame], h3_level: int) -> np.ndarray:
    """
    Calculates h3 index for the given resolution
    Parameters:
    h3_level: Resolution of the h3 Index
    df: Dataframe with latitude and longitude
    """

    def partition_wise(df):
        return da.asarray(vect_h3.geo_to_h3(df.latitude, df.longitude, h3_level))

    if isinstance(df, pd.DataFrame):
        return vect_h3.geo_to_h3(df.latitude, df.longitude, h3_level)
    elif isinstance(df, dd.DataFrame):
        return df.map_partitions(partition_wise)


def weather_to_mag_and_dir(df):
    def magnitude(u, v):
        return (np.pi / 180) * np.sqrt((np.square(u) + np.square(v)))

    def direction(u, v):
        return 180 + (180 / np.pi * np.arctan2(u, v))

    current_u, current_v = (
        df["eastward_sea_water_velocity_unit_m_s-1"],
        df["northward_sea_water_velocity_unit_m_s-1"],
    )
    wind_u, wind_v = df["eastward_wind_unit_m_s-1"], df["northward_wind_unit_m_s-1"]
    df["wind_magnitude"] = magnitude(wind_u, wind_v)
    df["wind_direction"] = direction(wind_u, wind_v)
    df["current_magnitude"] = magnitude(current_u, current_v)
    df["current_direction"] = direction(current_u, current_v)
    return df


def total_wave_period(df):
    df["total_wave_period"] = (
        df["sea_surface_primary_swell_wave_mean_period_unit_s"]
        + df["sea_surface_primary_swell_wave_mean_period_unit_s"]
        + df["sea_surface_wind_wave_mean_period_unit_s"]
    )
    return df