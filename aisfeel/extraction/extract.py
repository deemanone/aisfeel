from typing import Union
from aisfeel.extraction import (
    ais_calculators,
    spatial_calculators,
    timeseries_calculators,
)
import pandas as pd
import dask.dataframe as dd


def extract_all(df: Union[pd.DataFrame, dd.DataFrame]):
    return (
        df
        # .reset_index(drop=True)
        .pipe(ais_calculators.ais_one_hot_encoding)
        .pipe(ais_calculators.u_v_ship)
        .pipe(ais_calculators.angle_to_vec)
        .pipe(ais_calculators.drift_angle)
        .pipe(ais_calculators.is_sailing)
        .pipe(ais_calculators.normalize_size)
        .pipe(spatial_calculators.calc_hex_res)
    )